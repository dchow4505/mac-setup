#!/usr/bin/env bash

# installs Beanworks software stack at ~/beanworks
cd ~
mkdir beanworks
cd beanworks
git clone https://github.com/beanworks/dcm.git
cd dcm

# installs a 'Mac-friendly' bean.yml in ~/beanworks/dcm
git clone git@bitbucket.org:beanworks/beanyml.git
beanyml/setup.sh

# source beanworks config
cd ~/beanworks/dcm
source ~/setup/beanworks.include.sh

brew install docker
brew install docker-machine
brew install docker-compose

echo "Update your shell profile (eg. .zshrc) with 'source ~/setup/beanworks.include.sh"
echo "Now you can run dcm setup"
