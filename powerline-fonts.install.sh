#!/usr/bin/env bash

# install powerline fonts
git clone https://github.com/powerline/fonts.git ~/fonts
~/fonts/install.sh
rm -rf ~/fonts

# install powerlevel9k
brew tap sambadevi/powerlevel9k
brew install powerlevel9k
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

echo '
ZSH_THEME="powerlevel9k/powerlevel9k"

# prompt adjustment
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(root_indicator time)' >> ~/.zshrc