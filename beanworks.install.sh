#!/usr/bin/env bash

echo "source ~/setup/beanworks.include.sh" >> ~/.zshrc
echo "source ~/setup/java.include.sh" >> ~/.zshrc
source ~/.zshrc

dcm setup
cd srv

mkdir bean
cd bean

git clone git@bitbucket.org:beanworks/beanworksapi.git api
git clone git@bitbucket.org:beanworks/beanworksui.git ui
git clone git@bitbucket.org:beanworks/beanauth.git auth
git clone git@bitbucket.org:beanworks/beanlogger.git logger
git clone git@bitbucket.org:beanworks/service-haproxy.git lb
git clone git@bitbucket.org:beanworks/shard-trebuchet.git treb

brew install git-lfs
git clone git@bitbucket.org:beanworks/synctool_v2.git

dcm goto auth
mvn clean install -DskipTests