#!/usr/bin/env bash

# Is there anything lingering
brew cleanup

# Tap third-party formulae
brew tap homebrew/cask-versions

# Update Repositories
brew update

# Java #
brew cask install adoptopenjdk8
brew install maven
brew install gradle